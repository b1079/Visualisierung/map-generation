import dataclasses

from basicclasses.GeoJsonPoint import GeoJsonPoint
from basicclasses.Address import Address


@dataclasses.dataclass
class GenericLocationInformation:
    location: GeoJsonPoint
    address: Address
