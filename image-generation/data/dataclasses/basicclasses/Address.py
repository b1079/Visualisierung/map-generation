import dataclasses


@dataclasses.dataclass
class Address:
    houseNumber: str
    street: str
    city: str
    country: str
    postalCode: str
    state:str
