import dataclasses


@dataclasses.dataclass
class GeoJsonPoint:
    latitude: float
    longitude: float
