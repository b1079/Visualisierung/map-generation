import dataclasses
from basicclasses.GeoJsonPoint import GeoJsonPoint
@dataclasses.dataclass
class CityTestingData:
    id: str
    originalRequest: dict
    correctAcceptedApi: str
    geocodingPoint: GeoJsonPoint
    requests: int
    requestsForCity: int
    isCorrect: bool
    otherApis: list
    otherRequests: list
    randomCalls: int
    correctedLevel: int
    corrected: dict



