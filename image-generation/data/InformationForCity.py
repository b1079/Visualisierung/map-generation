import requests


class InformationForCity:
    def __init__(self, city_name):
        self.data = {}
        self.name = city_name
        self.request_data()

    def request_data(self):
        endpoint = "http://localhost:10001/pointsForCity"
        params = {'city': self.name}
        r = requests.get(endpoint, params)
        self.data = r.json()
        return self


