# Visualsierung in Karten

## Inhalt
Dieses Modul enthält die Jupyter Notebooks zum Erstellen der Karten. Ebenso werden mithilfe dieser Excel Dateien zum händischen
Auslesen generiert. Des weitern enthält es das Jupyter Notebook zum Auslesen der Datei des Statistischen Bundesamtes.

## Generierte Bilder
Im Ordner ``image-generation`` sind zusätzlich zu den Jupyter Notebooks ebenfalls die Bilder enthalten, welche generiert wurden.

